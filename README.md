# VPC Module

Original project by: https://github.com/cloudposse/terraform-aws-vpc.git

## Examples

```hcl
module "vpc" {
  source     = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc.git?ref=0.0.1"
  namespace  = "cp"
  stage      = "prod"
  name       = "app"
  cidr_block = "10.0.0.0/16"
}
```

Full example https://gitlab.com/nutellino-playground/tf-modules/vpc-subnet.git : 

```hcl
module "vpc" {
  source     = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc.git?ref=0.0.1"
  namespace  = "cp"
  stage      = "prod"
  name       = "app"
  cidr_block = "10.0.0.0/16"
}

module "dynamic_subnets" {
  source             = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc-subnet.git?ref=0.0.1"
  namespace          = "cp"
  stage              = "prod"
  name               = "app"
  region             = "us-west-2"
  availability_zones = ["us-west-2a","us-west-2b","us-west-2c"]
  vpc_id             = "${module.vpc.vpc_id}"
  igw_id             = "${module.vpc.igw_id}"
  cidr_block         = "10.0.0.0/16"
}
```

